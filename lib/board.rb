class Board

  attr_accessor :grid

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def initialize (grid = Board.default_grid)
    @grid = grid

  end

  def valid?(pos)
    return false if pos.is_a? Array == false
    rows = @grid.size
    cols = @grid[0].count
    return false if pos[0].between?(0, rows - 1) == false
    return false if pos[1].between?(0, cols - 1) == false
    true

  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def won?
    @grid.flatten.none?{|mark| mark == :s}
  end

  def place_random_ship
    raise "Already Full" if self.full?
    rows = @grid.size
    cols = @grid[0].count
    placed = false
    until placed == true
      row = rand(0...rows)
      col = rand(0...cols)
      if empty?([row, col])
        @grid[row][col] = :s
        placed = true
      end


    end


  end

  def full?
    @grid.flatten.all? {|mark| mark == :s}
  end

  def empty?(pos = nil)
    if pos == nil
      @grid.flatten.none? {|mark| mark == :s}
    else
      self[pos].nil?
    end
  end

  def count
    counter = 0
    @grid.each do |array|
      array.each do |mark|
        counter +=1 if mark == :s
      end
    end
    counter
  end

  def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end


end
