class HumanPlayer
  attr_reader :player, :board

  def initialize(player)
    @player = player

  end


  def get_play
    coordinates = nil
    until valid?(coordinates)
      puts "Enter the coordinates you wish to attack exp (0,0)"
      coordinates = gets.strip.split(",")
      coordinates.map!(&:to_i)
    end
    coordinates

  end
end
