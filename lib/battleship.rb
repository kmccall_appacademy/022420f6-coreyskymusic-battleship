class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board = Board.new)
    @player = player
    @board = board

  end

  def play_turn
    target = player.get_play
    attack(target)

  end


  def attack(pos)
    board[pos] = :x
  end

  def count
    @board.count(:s)
  end

  def game_over?
    @board.won?
  end
end
